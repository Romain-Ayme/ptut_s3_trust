\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}

\title{Documentation edDSA (Edwards-curve Digital Signature Algorithm)}
\date{Decembre 2020}

\begin{document}

\maketitle
\tableofcontents
\newpage


\section{Introduction}
EdDSA est un algorithme de signature numérique moderne et sécurisé basé sur les courbes elliptiques optimisées pour les performances, telles que la courbe de 255 bits Curve25519 et la courbe de 448 bits Curve448-Goldilocks. Les signature EdDSA utilisent la forme Edwards des courbes elliptiques ( pour des raisons de performances), respectivement edwards25519 et edwards 448. L’algorithme EdDSA est basé sur l’algorithme de signature de Schnorr et repose sur la difficulté du problème du logarithme discret permettant ainsi de pallier aux soucis de sécurité que l’on pouvait trouver dans d’autres fonctions de signature numérique. 
Ed25529 et Ed448 sont des instances  spécifiques de la famille EdDSA et sont spécifiés dans la RFC 8032.\newline
\begin{center}
  \includegraphics{courbe_edward_tordue.png} \newline
  Courbe d’Edwards tordue d’équation : 
\end{center}

\[10x^2 + y^2 = 1+ 6x^2y^2\]
\newpage

\section{Paramètre de la courbe}

La courbe edDSA admet 11 paramètres pour “fonctionner” : \newline
\begin{enumerate}
    \item 

    \item un entier $p$ premier impair (permet de savoir sur quel groupe fini il faut se placer : ici $GF(p)$ ou plus généralement $\mathbb{Z}/p\mathbb{Z}$)
    \item un entier $b$ avec $2^{b-1} > p$. Il permet de définir la taille de la clé publique ($b$ bits) ainsi que la signature ($2*b$ bits). Il est recommandé d’avoir un $b$ multiple de $8$ pour avoir des octets finis
    \item un encodage de $b-1$ bits pour les éléments sur $GF(p)$
    \item une fonction de hachage qui ressort $b-1$ bits. Il est préférable d’utiliser une fonction de hachage qui ne crée pas de collision
    \item un entier $c$ qui prend la valeur 2 ou 3. Le scalaire secret de edDSA (obtenue en hachant la clé privée avec sha512. Ce hash va générer 2 secrets : le premier est celui du scalaire secret et le second est utilisé pour le schéma de signature) est multiple de $2^c$. De plus, $c$ est le logarithme en base 2 du cofacteur (voir génération des clés pour plus de précision sur le cofacteur).
    \item un entier $n$ avec $c <= n < b$. Le scalaire secret de edDSA a exactement $n+1$ bits
    \item un entier $d$ non-carré (ex: 25 est un nombre carré mais 5 ne l’est pas car la racine  carré de 5 n’est pas un nombre entier), le plus proche de zéros et qui donne une courbe acceptable
    \item un entier $a$ carré et non-nul. Il est recommandé de prendre $a = 1$ quand $p mod 4 =1$ et $a = -1$ quand $p mod 4 = 3$
    \item un élément $B$ qui n’est pas $0$ et $1$ et qui appartient à l’ensemble $E$ définit comme $(x,y)$ un membre de $GF(p)$ ou $x$ appartient à $GF(p)$ et vérifie l’équation $a * x^2 y^2 = 1 + d * x^2 * y^2$
    \item un entier $L$ tel que $[L]B = 0$ et $2^c * L = E$ ou $E$ est le nombre de points sur la courbe  
    \item une fonction de préhash \newline
\end{enumerate}


On a donc les paramètres suivant pour la courbe ed25519 : \newline
\begin{itemize}

    \item $p = 2255 - 19$
    \item $b = 256$
    \item $255$ bits d’encodage
    \item sha-512 pour la fonction de hachage
    \item $c = 3$
    \item $n = 254$ 
    \item $d = 37095705934669439343138083508754565189542113879843219016388785533085940283555$
    \item $a = -1$
    \item $B = X(p),Y(p) =$ \newline
    $15112221349535400772501151409588531511454012693041857206046113283949847762202$,\newline
    $46316835694926478169428394003475163141307993866256225615783033603165251855960$
    \item $L = 2252+27742317777372353535851937790883648493$
    \item $PH(x) = x$
\end{itemize}    

\section{Génération de clés EdDSA}

Ed25519 et Ed448 utilisent en même temps de petites clés privées et clés publiques (32 ou 57 octets) et de petites signatures (64 ou 114 octets) avec un niveau de sécurité élevé (respectivement 128 bits ou 224 bits).\newline
En cryptographie, une courbe elliptique est un sous-groupe qui a une taille donnée n. Nous travaillons normalement dans un sous-groupe d’ordre premier r, où r divise n. Soit h un entier, le cofacteur est égal à  h = n / r. Pour chaque point P sur la courbe, le point hP est soit le “point à l’infini”, soit il est de l’ordre de r, c’est-à-dire qu’en prenant un point, le multiplier par le cofacteur donne nécessairement un point dans le sous-groupe d’ordre premier r. (Le cofacteur est important dans la mesure où il n’est pas égal à 1)

Supposons que la courbe elliptique pour l’algorithme EDDSA est fourni avec un point de générateur \textbf{G} et un sous-groupe afin de \textbf{q} pour les points de la courbe elliptique, générée à partir de \textbf{G}. 
La paire de clés EdDSA se compose de : \newline

\begin{itemize}
    \item \textbf{clé privée} (entier) : privKey
    \item \textbf{public key} (Point de la courbe elliptique) : pubKey = privKey * G \newline
\end{itemize}

La \textbf{clé privée} est générée à partir d’un entier aléatoire, appelé “seed” (qui doit avoir une longueur en bits similaire , comme l’ordre des courbes). La seed est d’abord hachée, puis les derniers bits, correspondant au cofacteur de la courbe \textbf{(8 pour Ed25519 et 4 pour Ed448 afin de garantir que les points seront dans le sous-groupe approprié)} sont effacé et le deuxième bit le plus élevé est mis à 1. 
Ces transformations garantissent que la clé privée appartiendra toujours au même sous-groupe de points de la courbe elliptique et que les clés privées auront toujours une longueur de bits similaire (pour se protéger des attaques par canal latéral basées sur la synchronisation).
Pour Ed25519, la clé privée est de 32 octets (57 pour Ed448).

La \textbf{clé publique} $PubKey$ est un point sur la courbe elliptique, calculé par la multiplication de points de la courbe elliptique : $pubKey = priv Key * G$  (la clé privée, multipliée par le point générateur $G$ de la courbe). La clé publique est codée en tant que point de la courbe elliptique compressé : la coordonnée $y$, combinée avec le bit le plus bas (la parité) de la coordonnée $x$.
Comme pour la clé privée, la clé publique est de $32$ octets pour Ed25519 et $57$ octets pour Ed448.

\section{Signer EdDSA}

L’algorithme de signature EdDSA prend en entrée un message \textbf{texte msg + la clé privée} EdDSA du signataire privKey et produit en sortie une paire d’entiers $\{R,s\}$. 
La signature EdDSA fonctionne de la manière suivante (avec quelques simplifications) :

$EdDSA\_sign(msg, privKey) \rightarrow {R, s }$

\begin{enumerate}
    \item On calcule \textbf{$pubKey = privKey*G$}
    \item On génère un entier secret \textbf{$r = hash$} (hash(privKey) + msg)  \textbf{$\mod q$}
    \item On calcule le point de la clé publique \textbf{$R = r*G$} 
    \item On calcule \textbf{$h = hash$} (R + pubKey + msg) \textbf{$\mod q$}
    \item On calcule \textbf{$s = ( r + h * privKey) \mod q$}
    \item On renvoie la signature \textbf{${R, s}$}
\end{enumerate}


La signature numérique produite vaut 64 octets (32+32 octets) pour Ed25519 et 114 octets (57+57 octets) pour Ed448. Il contient un point compressé \textbf{$R$} + l’entier \textbf{$s$} (confirmant que le signataire connaît le msg et la clé privée).

\section{Signature de vérification EdDSA} 

L’algorithme de vérification de signature EdDSA prend en entrée un message texte\textbf{ msg + la clé publique}  EdDSA pubKey du signataire + la signature EdDSA \textbf{$\{R,s\}$} et produit en sortie une valeur booléenne pour dire si la signature est valide ou non. 
La vérifiaction EdDSA fonctionne de la manière suivante (avec quelques simplifications) : 

$EdDSA\_signature\_verify(msg, pubKey, signature {R,s}) \rightarrow valid / invalid$
\begin{enumerate}
    \item On calcule \textbf{$h = hash$} (R + pubKey + msg) \textbf{$\mod q$}
    \item On calcule \textbf{$P1 = s * G$} 
Lors de la signature on a fait le calcul de \textbf{$s = ( r + h * privKey) \mod q$}. Il nous suffit de le remplacer dans l’équation $P1$, ce qui nous donne : 

\textbf{$P1 = (r + h * privKey) \mod q * G$} 
On développe \textbf{$P1 = r * G + h * privKey * G$} 
Or \textbf{$r * G = R$} et \textbf{$privKey * G = pubKey$}  
Donc \textbf{$P1 = R + h * PubKey$}
    \item On calcule \textbf{$P2 = R + h * pubKey$}
    \item En retour, on va tester \textbf{$P1==P2$}
Si ces points $P1$ et $P2$ sont le même point dans la courbe elliptique, cela prouve que le point $P1$, calculé par la clé privée, correspond au point $P2$, créé par sa clé publique correspondante.
\end{enumerate}
\newpage
\section{EdDSA vs ECDSA} 

Si nous comparons la signature et la vérification pour EdDSA, on peut constater qu’ EdDSA est plus facile à comprendre et à mettre en œuvre que ECDSA. Toutefois, les deux algorithmes de signature ont une force de sécurité similaire pour les courbes avec des longueurs de clé similaires. 
Pour les courbes les plus connues  ( edwards25519 et edwards448), l’algorithme EdDSA est légèrement plus rapide que l’ECDSA, mais cela dépend grandement des courbes utilisées et de l’implémentation. 
Par ailleurs, contrairement à ECDSA, les signatures EdDSA ne permettent pas de récupérer la clé publique du signataire à partir de la signature et du message. En général, on considère qu’EdDSA est recommandée pour la plupart des applications modernes. 


\section{Sources}
 
paramètre de la courbe edDSA : \href{https://tools.ietf.org/html/rfc8032}{https://tools.ietf.org/html/rfc8032}\newline
scalaire secret : \href{https://blog.filippo.io/using-ed25519-keys-for-encryption/}{https://blog.filippo.io/using-ed25519-keys-for-encryption/} \newline (paragraphe “So that's what a X25519 public key ...)\newline

\href{https://crypto.stackexchange.com/questions/56344/what-is-an-elliptic-curve-cofactor}{https://crypto.stackexchange.com/questions/56344/what-is-an-elliptic-curve-cofactor}\newline
\href{https://crypto.stackexchange.com/questions/51350/what-is-the-relationship-between-p-prime-n-order-and-h-cofactor-of-an-ell?noredirect=1&lq=1}{https://crypto.stackexchange.com/questions/51350/what-is-the-relationship-between-p-prime-n-order-and-h-cofactor-of-an-ell?noredirect=1&lq=1}\newline
\href{https://cryptologie.net/article/497/eddsa-ed25519-ed25519-ietf-ed25519ph-ed25519ctx-hasheddsa-pureeddsa-wtf/}{https://cryptologie.net/article/497/eddsa-ed25519-ed25519-ietf-ed25519ph-ed25519ctx-hasheddsa-pureeddsa-wtf/}\newline
\href{https://crypto.stackexchange.com/questions/50405/summarize-the-mathematical-problem-at-the-heart-of-breaking-a-curve25519-public/50414#50414}{https://crypto.stackexchange.com/questions/50405/summarize-the-mathematical-problem-at-the-heart-of-breaking-a-curve25519-public/50414#50414}\newline

Implémentation : \href{https://www.oryx-embedded.com/doc/ed25519\_8c\_source.htm}{https://www.oryx-embedded.com/doc/ed25519\_8c\_source.htm}
\end{document}
