\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{pgfplots}

\pgfplotsset{compat=1.16}

\begin{document}
\section{Cracking hors ligne}

\textit{Les durées d'exécutions sont des moyennes, nous avons effectué les tests avec la configuration suivante : } \\
\begin{itemize}
	\item Processeur : Intel Core i7-8550U 1.80GHz \(\times\) 8
	\item RAM : 8 Go DDR4 2400Mhz
	\item Debian 10 Gnome
	\item Valgrind et massif
\end{itemize}

\subsection{Introduction}

En théorie, les mots de passe devraient être stockés dans les bases de données d'une manière précise. Par exemple, la CNIL recommande l'utilisation d'un algorithme cryptographique non réversible (donc pas de chiffrement tel que RSA, etc), pour lequel on ne connaît pas de failles de sécurité et combiné avec du salage différent pour chaque utilisateur (empêche les attaque à partir de table pré-calculées). Ce salage ne devrait pas être conservé "dans le même espace de stockage que l'élément de vérification du mot de passe". \\

Le problème majeur ici, est que les fonctions de hachage telles que SHA-256, SHA-1 ou MD5 sont trop rapides à exécuter.
Cela peut paraître paradoxal mais dans le cas du stockage de mots de passe sous forme hachée, la vérification ne devrait être faite qu'au plus quelques fois par jour.
Il est évident qu'une fonction trop lente serait encombrante pour l'utilisateur et pour le serveur chargé de vérifier les mots de passe, mais une fonction trop rapide permettrait à un attaquant qui parviendrait à se procurer le haché, de tester un nombre important de combinaisons en un temps relativement court. En d'autres termes : au plus la fonction s'exécute rapidement, au plus l'attaque par force brute est efficace. En général, des algorithmes tels que bcrypt, scrypt ou PBKDF2 sont recommandés car ils sont plus lents grâce à un nombre d'itérations variable pour ajuster le temps d'exécution (plus lent que l'usage simple de SHA-256, SHA-1 ou encore MD5).

Ici, nous allons nous intéresser aux cas dans lesquels ces recommandations ne sont pas respectées : pas de salage et une simple utilisation de SHA-256 ou MD5. De plus, nous utiliserons des mots de passe courts afin de faciliter les tests. Dans un premier temps, nous allons essayer d'utiliser la méthode de force brute avec SHA-256 puis nous mesurerons le temps d'exécution pour retrouver le même mot de passe haché avec MD5 seulement, qui est significativement plus rapide à calculer. D'autres méthodes seront utilisées au S4.

\subsection{Définition de l'alphabet utilisé}

Pour générer toutes ces combinaisons, il faut tout d'abord définir les caractères utilisés. Notre alphabet se composera de l'alphabet commun (de a à z) en minuscule et en majuscule, des chiffres de 0 à 9 et de quelques caractères spéciaux.

\begin{verbatim}
<space>!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
\end{verbatim}
\begin{verbatim}
0123456789
\end{verbatim}
\begin{verbatim}
ABCDEFGHIJKLMNOPQRSTUVWXYZ
\end{verbatim}
\begin{verbatim}
abcdefghijklmnopqrstuvwxyz
\end{verbatim}

On dispose de 95 caractères différents, ce qui donne \(95^2 = 9025\) mots de passe sur 2 caractères, \(95^3 = 857375\) sur 3 caractères, plus de \(80\) millions de possibilités sur 4 caractères...


\subsection{Première implémentation}

Dans une première implémentation nous générons, en les stockant dans un vecteur, tout les mots possibles avec 1 caractère (\(\{a,b,c,...\}\)), puis hachons et comparons avec la valeur cible. Si le résultat n'a pas été trouvé, nous effectuons les tests avec tous les mots sur 2 caractères... \\

Les points faibles de cette implémentation sont les suivants :
\begin{itemize}
    \item Le stockage est trop important : nous stockons tous les mots depuis le début du programme ce qui n'est pas utile car certains mots ont déjà été testés, il faudrait mettre à jour ce conteneur pour ne pas perdre de place inutilement. Pour un mot de passe suffisamment long, la taille maximale du vecteur serait atteinte et poserait problème.
    
    \item Les relocalisations en mémoire du vecteur sont très pénalisantes.
    
    \item Nous sommes obligés de générer tous les mots de \(n\) caractères avant de pouvoir les tester : en effet nous continuons la génération même si le bon mot se trouve déjà dans notre conteneur. Par exemple, si le message que nous cherchons est : "!" nous sommes obligés de créer tous les mots d'un caractère, avant de pouvoir savoir que le message était "!", nous avons donc 95 mots alors qu'il aurait été possible d'en générer seulement 2 si l'on prend les caractères dans l'ordre de la table ASCII. Ce problème devient de plus en plus conséquent lorsque le nombre de caractères grandit.
\end{itemize}

Pour le mot de passe "hash", \(30\) secondes ont été nécessaires et le pic d'utilisation de la mémoire était de \(5,7\) Go. Ce résultat est évidemment loin d'être satisfaisant.

\subsection{Deuxième implémentation}

Nous avons conservé le principe de génération des mots, mais cette fois ci, l'espace mémoire nécessaire est réservé avant la génération des mots sur \(n\) caractères. Ceci a pour principal objectif d'éviter les relocalisations du vecteur en mémoire. \\

De plus, nous avons vu que certains mots que nous générons ne sont jamais utilisés car le résultat est trouvé avant de les tester. Nous testons maintenant les mots dès leur génération.
Pour limiter l'utilisation de mémoire, nous effaçons également les mots qui ne sont plus utilisés : une fois les mots de taille \(n \geq 2\) générés, les mots de taille \(n - 1\) ne sont plus utiles. \\

Comme attendu, les résultats obtenus sont meilleurs : \(23\) secondes et \(4.9\) Go au maximum. De plus, avec le même algorithme, l'utilisation de MD5 à la place de SHA-256 simplifie grandement la tâche. D'après nos tests, ce sont \(16\) secondes qui sont nécessaires pour retrouver le mot de passe initial. La nécessité d'utilisé des fonctions "lentes", bien que contre-intuitive, est facilement compréhensible lorsque l'on observe de telles différences sur seulement \(4\) caractères.

\subsection{Version récursive}

Par l'utilisation d'une solution récursive, nous ne sommes plus obligés de stocker toutes les tentatives. De ce fait, l'utilisation mémoire est bien inférieure à celle que nous avions précédemment. De plus, Cette nouvelle implémentation est significativement plus rapide. Retrouver le mot de passe "hash" ne nécessite plus que \(2\) secondes et \(72\) Ko de mémoire. Nous sommes également parvenus à retrouver un mot de passe sur 5 caractères alors que cela était impossible avec la version précédente du fait de l'utilisation excessive de la mémoire (plus de 30 Go avant plantage).

\subsection{Recursivité et multithreading}

Afin d'abaisser encore le temps d'exécution de notre programme, nous avons également développé une version reposant sur le multithreading. L'idée est simple : pour une machine capable d'exécuter \(n \geq 2\) threads, nous en utilisons \(n - 1\) pour générer les chaînes de caractères et les tester et le dernier est chargé de distribuer les tâches aux autres threads. La répartition choisie est simple, pour \(n - 1\) threads utilisés pour les calculs, le premier génère et teste les chaînes sur \(1\) caractère, le second fait ceci pour les chaînes sur \(2\) caractères... Le \((n - 1)^{eme}\) se charge des chaînes sur \(n - 1\) caractères. Lorsqu'un thread termine les tests sur toutes les combinaisons possible pour le nombre de caractères qui lui est assigné, le thread "principal" lance un nouveau thread qui se charge des chaînes sur \(n\) caractères... \\

Nous avons décidé de limiter le nombre de threads au nombre que le processeur peut réellement exécuter en parallèle (déterminé par un appel à \\
\verb|thread::hardware_concurrency()| afin de conserver une situation de parallélisme réel. Il serait cependant intéressant de mener des expérimentations poussées afin de chiffrer les écarts entre les deux approches.

\begin{center}
	\includegraphics[scale=0.35]{../ressources/hardwareConcurrency.png}
\end{center}

Voici un exemple d'exécution possible de notre programme avec une machine capable d'exécuter 8 threads en parallèle et un mot de passe sur \(4\) caractères (les affichages peuvent apparaître dans un ordre illogique du fait de la parallélisation) :

\begin{verbatim}
	debut -- thread -> taille : 1
	debut -- thread -> taille : 2
	fin   -- thread -> taille : 1
	debut -- thread -> taille : 3
	debut -- thread -> taille : 4
	debut -- thread -> taille : 5
	debut -- thread -> taille : 6
	debut -- thread -> taille : 7
	Attente de libération d'un thread
	7 / 7
	----- 
	debut -- thread -> taille : 8
	Attente de libération d'un thread
	7 / 7
	-----
	debut -- thread -> taille : 9
	fin   -- thread -> taille : 2
	Attente de libération d'un thread
	7 / 7
	----- 
	fin   -- thread -> taille : 3
	debut -- thread -> taille : 10
	Attente de libération d'un thread
	7 / 7
	----- 
	fin   -- thread -> taille : 4
	fin   -- thread -> taille : 9
	fin   -- thread -> taille : 7
	fin   -- thread -> taille : 8
	fin   -- thread -> taille : 6
	fin   -- thread -> taille : 5
	fin   -- thread -> taille : 10
\end{verbatim}

Tous les threads se terminent (sans poursuivre leur tâche) après que le mot de \(4\) caractères ait été trouvé (le temps d'exécution est de 2 secondes et le pic d'utilisation de la mémoire se situe à 74 Ko).

\subsection{Comparaison avec Hashcat}

\textit{À partir de cette section, les tests sont effectués avec une machine différente. Il est donc possible que les résultats varient par rapport à ce qui a été présenté précédemment.} \\

Afin d'avoir une idée des performances de notre programme, nous avons effectué quelques tests comparatifs avec l'outil hashcat. La question n'était pas ici de déterminer lequel de ces deux programme était plus rapide, mais de savoir à quel point nous étions plus lents de hashcat.

\begin{center}
	\begin{tikzpicture}
	\begin{axis}
	[
	title={Durée d'une attaque de préimage (jusqu'à 4 caratères)},
	xlabel={Taille du mot de passe (caractères dans l'alphabet décrit plus haut)},
	ylabel={durée (en secondes)},
	xmin=1, xmax=4,
	ymin=0, ymax=2,
	xtick={1, 2, 3, 4},
	ytick={0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2},
	legend pos=north west,
	ymajorgrids=true,
	grid style=dashed,
	scale=1
	]
	\addplot[smooth]
	[
	color=red,
	mark=*,
	]
	coordinates
	{
		(1, 0.0)(2, 0.15)(3, 0.31)(4, 0.52)
	};
	
	
	\addplot[smooth]
	[
	color=blue,
	mark=*,
	]
	coordinates
	{
		(1, 0.0)(2, 0.001)(3, 0.021)(4, 1.820)
	};
	
	\legend{Hashcat, Notre programme}
	\end{axis}
	\end{tikzpicture}
\end{center}

\begin{center}
	\begin{tikzpicture}
	\begin{axis}
	[
	title={Durée d'une attaque de préimage (jusqu'à 5 caratères)},
	xlabel={Taille du mot de passe (caractères dans l'alphabet décrit plus haut)},
	ylabel={durée (en secondes)},
	xmin=1, xmax=5,
	ymin=0, ymax=175,
	xtick={1, 2, 3, 4, 5},
	ytick={20, 40, 60, 80, 100, 120, 140, 160},
	legend pos=north west,
	ymajorgrids=true,
	grid style=dashed,
	scale=1
	]
	\addplot[smooth]
	[
	color=red,
	mark=*,
	]
	coordinates
	{
		(1, 0.0)(2, 0.15)(3, 0.31)(4, 0.52)(5, 54)
	};
	
	
	\addplot[smooth]
	[
	color=blue,
	mark=*,
	]
	coordinates
	{
		(1, 0.0)(2, 0.001)(3, 0.021)(4, 1.820)(5, 170)
	};
	
	\legend{Hashcat, Notre programme}
	\end{axis}
	\end{tikzpicture}
\end{center}

\begin{center}
	\begin{tabular}{ |c|c|c|c|c|c| } 
		\hline
		Taille du mot de passe & 1 & 2 & 3 & 4 & 5 \\
		\hline
		hashcat & 0.0 & 0.15 & 0.31 & 0.52 & 54 \\
		\hline
		notre programme & 0.0 & 0.0 & 0.021 & 1.820 & 170 \\ 
		\hline
	\end{tabular} \\
	Les temps sont en secondes
\end{center}

Sur des mots de passes courts, notre programme est plus rapide que hashcat, mais la tendance s'inverse au delà de 3 caractères. À partir de cette valeur, la différence de performance est abyssale.

\subsection{Conclusion}

Comme nous l'avons vu le "crackage" par force brute s'avère extrêmement long et coûteux pour nos machines, surtout lorsque le temps de calcul des fonctions de hachage est long (on comprend aisément pourquoi il ne faut pas utiliser SHA-256 ou MD5 par exemple) et que les mots de passe deviennent "longs" avec une augmentation significative du temps d'exécution au delà de 5 caractères (plusieurs heures estimées par haschat pour un 6 caractères). De plus, de petites optimisations ne suffisent pas à le rendre efficace. Contrairement à certains programme courants qui "supportent relativement bien" une implémentation peu performante, l'efficacité de cette application dépend grandement des choix effectués lors du développement. \\

Lors de nos tests, nous avons décelé l'une des faiblesses les plus importantes de notre programme : sa consommation de mémoire. Cela n'est pas étonnant dans la mesure où nous stockons un nombre exponentiel de chaînes (pour un mot de passe dont la longueur est au delà de 4 caractères, l'utilisation mémoire dépasse les 20 Go). Une refonte totale du programme a été nécessaire pour atteindre des performances correctes en terme de mémoire et de temps d'exécution. Cependant, lorsque la taille des mots de passe grandit, notre programme accuse un retard très important sur hashcat (environ 3 fois plus lent pour 5 caractères). Notons cependant que du point de vue de l'utilisation de mémoire, hashcat est moins performant avec un pic à environ 2Go pour un mot de passe de 4 caractères. Cela reste anecdotique dans la mesure où son utilisation mémoire est tout a fait convenable et que son approche lui permet d'être très efficace sur le plan du temps d'exécution. \\


\end{document}
