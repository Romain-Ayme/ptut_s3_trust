#ifndef RSAPRIVATEKEY_H
#define RSAPRIVATEKEY_H

#include <memory>
#include <gmpxx.h>
#include "rsakey.h"
#include "rsapublickey.h"

namespace nsCrypto
{
    class RSAPrivateKey : public RSAKey
    {
        mpz_class m_p;
        mpz_class m_q;
    public:
        explicit RSAPrivateKey(unsigned long bits);
        RSAPrivateKey(const mpz_class& p, const mpz_class& q);
        RSAPublicKey getAssociatedPublicKey() const;
        const mpz_class& get_p() const;
        const mpz_class& get_q() const;
        bool isPublicKeyValid(const std::shared_ptr<RSAPublicKey> &pk) const;
        void setP(const mpz_class& p);
        void setQ(const mpz_class& q);
    }; // class RSAPrivateKey
} // namespace nsCrypto

#endif // RSAPRIVATEKEY_H
