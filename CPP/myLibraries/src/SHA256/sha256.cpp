#include <sstream>
#include <fstream>
#include <iomanip>
#include "sha256.h"

using namespace std;

const array<uint32_t, 64> nsHash::SHA256::K{0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
                                           0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
                                           0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
                                           0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
                                           0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
                                           0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
                                           0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
                                           0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2};
const array<uint32_t, 8> nsHash::SHA256::registersInit{0x6a09e667,
                                                       0xbb67ae85,
                                                       0x3c6ef372,
                                                       0xa54ff53a,
                                                       0x510e527f,
                                                       0x9b05688c,
                                                       0x1f83d9ab,
                                                       0x5be0cd19};

nsHash::SHA256::SHA256(const string & input /* = string() */)
    : HashFunction(input), m_hash(registersInit)
{}

void nsHash::SHA256::computeString()
{
    resetHash();

    istringstream istr(m_inputData);
    compute(istr);
}

void nsHash::SHA256::computeFile()
{
    resetHash();

    ifstream file;
    file.exceptions(ios_base::failbit);
    file.open(m_inputData, ios_base::binary); // throw std::ios_base::failure si le fichier n'est pas ouvert
    file.exceptions(ios_base::goodbit); // désactiver les exceptions
    compute(file);
}

void nsHash::SHA256::compute(istream &stream)
{
    array<uint32_t, 16> tab;
    array<uint8_t, 16*4> tempTab;
    bool fin(false);
    bool add1(true);
    while(!fin)
    {
        unsigned i(0);
        char in;

        while(i < tempTab.size() && stream.get(in))
        {
            tempTab[i] = in;
            ++i;
        }

        if(/*!stream && */ i + 8 < tempTab.size())
        {
            stream.clear();
            addPadding(tempTab, i, stream.tellg()*8, add1);
            fin = true;
        }
        else if(i < tempTab.size())
        {
            tempTab[i] = uint8_t(1) << (sizeof(uint8_t)*8 - 1); // début du padding pour comptéter le bloc
            ++i;
            while(i < tempTab.size())
            {
                tempTab[i] = 0;
                ++i;
            }
            add1 = false;
        }

        tab = toTab(tempTab);

        //traitement principal
        array<uint32_t, 64> W;
        copy(tab.begin(), tab.end(), W.begin());
        for(unsigned j(16); j < W.size(); ++j)
            W[j] = PetitSigma1(W[j - 2]) + W[j - 7] + PetitSigma0(W[j - 15]) + W[j - 16];

        uint32_t a(m_hash[0]),
                b(m_hash[1]),
                c(m_hash[2]),
                d(m_hash[3]),
                e(m_hash[4]),
                f(m_hash[5]),
                g(m_hash[6]),
                h(m_hash[7]);

        for(unsigned j(0); j < 64; ++j)
        {
            uint32_t T1(h + Sigma1(e) + Ch(e, f, g) + K[j] + W[j]);
            uint32_t T2(Sigma0(a) + Maj(a, b, c));

            h = g;
            g = f;
            f = e;
            e = d + T1;
            d = c;
            c = b;
            b = a;
            a = T1 + T2;
        }
        m_hash[0] = a + m_hash[0];
        m_hash[1] = b + m_hash[1];
        m_hash[2] = c + m_hash[2];
        m_hash[3] = d + m_hash[3];
        m_hash[4] = e + m_hash[4];
        m_hash[5] = f + m_hash[5];
        m_hash[6] = g + m_hash[6];
        m_hash[7] = h + m_hash[7];
    }

    m_valid = true;
}

const array<uint32_t, 8>& nsHash::SHA256::getResult() const
{
    return m_hash;
}

std::array<uint8_t, 32> nsHash::SHA256::getResultBytes() const
{
    array<uint8_t, 32> result;
    for(unsigned i(0); i < result.size(); ++i)
        result[i] = uint8_t((m_hash[i / 4] & (uint32_t(255) << ((3 - i)*8))) >> ((3 - i)*8));
    return result;
}

std::string nsHash::SHA256::getResultAsString() const
{
    ostringstream ostr;
    ostr.fill('0');
    for(unsigned i(0); i < m_hash.size(); ++i)
        ostr << hex << setw(8) << m_hash[i];
    return ostr.str();
}

std::array<uint32_t, 16> nsHash::SHA256::toTab(const std::array<uint8_t, blocksSize / 8> &tab) const
{
    array<uint32_t, 16> res;
    for(unsigned i(0); 4*i < tab.size(); ++i)
    {
        res[i] = 0;
        for(unsigned j(0); j < 4 && (4*i) + j < tab.size(); ++j)
            res[i] = res[i] | (uint32_t(tab[(4*i) + j])) << (8*(sizeof(uint32_t) - (j + 1)));
    }

    return res;
}


//ajouter 100000...
void nsHash::SHA256::addPadding(array<uint8_t, 16 * 4>& tab, size_t pos, uint64_t l, bool add1) const
{
    if(add1)
    {
        tab[pos] = uint8_t(1) << (sizeof(uint8_t)*8 - 1); // 10000000...
        ++pos;
    }
    while((pos * (sizeof(uint8_t)*8) + 64) % blocksSize != 0)
    {
        tab[pos] = 0;
        ++pos;
    }

    for(unsigned i(0); i < 8; ++i) // ajout de la valeur de l à la fin sur 64 bits
    {
        uint64_t mask(255);
        mask <<= 64 - (8 * (i + 1));
        tab[pos + i] = (l & mask) >> (64 - (8 * (i + 1)));
    }
}

uint32_t nsHash::SHA256::Ch(uint32_t x, uint32_t y, uint32_t z) const
{
    return (x & y) ^ ((~x) & z);
}

uint32_t nsHash::SHA256::Maj(uint32_t x, uint32_t y, uint32_t z) const
{
    return (x & y) ^ (x & z) ^ (y & z);
}

uint32_t nsHash::SHA256::RotateRight(uint32_t val, unsigned short n) const
{
    n %= sizeof(val)*8;
    uint32_t mask((uint32_t(1) << n) - 1);
    return (val >> n) | ((val & mask) << ((sizeof(val)*8) - n));
}

uint32_t nsHash::SHA256::Sigma0(uint32_t x) const
{
    return RotateRight(x, 2) ^ RotateRight(x, 13) ^ RotateRight(x, 22);
}

uint32_t nsHash::SHA256::Sigma1(uint32_t x) const
{
    return RotateRight(x, 6) ^ RotateRight(x, 11) ^ RotateRight(x, 25);
}

uint32_t nsHash::SHA256::PetitSigma0(uint32_t x) const
{
    return RotateRight(x, 7) ^ RotateRight(x, 18) ^ (x >> 3);
}

uint32_t nsHash::SHA256::PetitSigma1(uint32_t x) const
{
    return RotateRight(x, 17) ^ RotateRight(x, 19) ^ (x >> 10);
}

void nsHash::SHA256::resetHash()
{
    m_valid = false;
    m_hash = registersInit;
}
