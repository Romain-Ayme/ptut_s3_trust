#include <iostream>
#include "sha1.h"

using namespace std;
// https://brilliant.org/wiki/secure-hashing-algorithms/

int main()
{
    nsHash::SHA1 h("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz");
    h.computeString();
    cout << h.getResultAsString() << endl;

    nsHash::SHA1 h2("fic.txt");
    h2.computeFile();
    cout << h2.getResultAsString() << endl;

    return 0;
}
