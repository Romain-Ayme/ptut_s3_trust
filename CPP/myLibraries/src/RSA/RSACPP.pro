TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include/RSA
INCLUDEPATH += ../../include/RSAException

LIBS += -lgmpxx -lgmp

SOURCES += \
        RSAException/rsaException.cpp \
        main.cpp \
        numericutils.cpp \
        rsa.cpp \
        rsakey.cpp \
        rsaprivatekey.cpp \
        rsapublickey.cpp

HEADERS += \
    numericutils.h \
    rsa.h \
    rsakey.h \
    rsaprivatekey.h \
    rsapublickey.h
