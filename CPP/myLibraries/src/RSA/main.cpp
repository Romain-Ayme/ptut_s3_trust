#include <iostream>

#include "gmpxx.h"
#include "stdio.h"

#include "rsa.h"
#include "rsaprivatekey.h"
#include "rsapublickey.h"

using namespace std;

int main()
{
    nsCrypto::RSAPrivateKey privKey(nsCrypto::RSAPrivateKey::createFromNSize(64));
    nsCrypto::RSA rsaPriv(make_shared<nsCrypto::RSAPrivateKey>(privKey));
    nsCrypto::RSA rsaPub(make_shared<nsCrypto::RSAPublicKey>(privKey.getAssociatedPublicKey()));

    shared_ptr<nsCrypto::RSAPublicKey> pubK(static_pointer_cast<nsCrypto::RSAPublicKey>((rsaPub.getKey())));

    cout << "p : " << privKey.getP() << endl <<
            "q : " << privKey.getQ() << endl <<
            "priv exp : " << privKey.getExponent() << endl;

    cout << "pub exp : " << pubK->getExponent() << endl;

    mpz_class dataToEncrypt(3);
    cout << rsaPriv.compute(rsaPub.compute(dataToEncrypt)) << endl;

    return 0;
}
