#include <chrono>
#include "numericutils.h"

using namespace std;

mpz_class nsNumericUtils::pgcd(mpz_class a, mpz_class b)
{
    while(a % b != 0)
    {
        mpz_class temp(b);
        b = a % b;
        a = temp;
    }
    return b;
} // pgcd

bool nsNumericUtils::isPrime(const mpz_class& val)
{
    if(val <= 0) return false;
    return mpz_probab_prime_p(val.get_mpz_t(), 50);
} // isPrime

mpz_class nsNumericUtils::modularExp(const mpz_class& a, const mpz_class& b, const mpz_class& mod)
{
    if(b == 0)
        return 1;
    if(b % 2 == 0)
        return (modularExp(a, b / 2, mod) * modularExp(a, b / 2, mod)) % mod;
    else
        return (a * modularExp(a, (b - 1) / 2, mod) * modularExp(a, (b - 1) / 2, mod)) % mod;
} // modularExp

mpz_class nsNumericUtils::randomCoprime(const mpz_class& val)
{
    if(val <= 2)
        return 0;
    gmp_randclass randGMP(gmp_randinit_mt);
    randGMP.seed(static_cast<unsigned long>(chrono::system_clock::now().time_since_epoch().count()));
    mpz_class r(randGMP.get_z_range(val - 2)); // génération [1, n-1]
    r = r + 2; // donne une valeur entre 2 et val - 1

    while(nsNumericUtils::pgcd(r, val) != 1)
    {
        r += 1;
        if(r >= val)
            r = 2;
    }
    return r;
} // randomCoprime()

pair<mpz_class, mpz_class> invRecurs(const mpz_class& val, const mpz_class& mod)
{
    pair<mpz_class, mpz_class> p;
    if(val == 0) // donc m == 1 car a et m sont premiers entre eux
        return {0, 1}; // 1 = (_0 * a) + (_1 * m);
    else
        p = invRecurs(mod % val, val); // 1 = x*(m%a) + y*a
    return {p.second - (mod / val) * p.first, p.first};
} // invRecurs

mpz_class nsNumericUtils::modularInverse(const mpz_class& val, const mpz_class& mod)
{
    if(pgcd(val, mod) != 1)
        throw invalid_argument("valeur non inversible");
    pair<mpz_class, mpz_class> p = invRecurs(val, mod);
    return (p.first < 0 ? p.first + mod : p.first);
} // modularInverse

mpz_class nsNumericUtils::randomPrime_n_bits(unsigned long bits)
{
    gmp_randclass randGMP(gmp_randinit_mt);
    randGMP.seed(static_cast<unsigned long>(chrono::system_clock::now().time_since_epoch().count()));
    mpz_class minLimit;
    mpz_class maxLimit;
    mpz_class range;

    mpz_ui_pow_ui(minLimit.get_mpz_t(), 2, bits - 1);
    minLimit += 1; // 100...001

    mpz_ui_pow_ui(maxLimit.get_mpz_t(), 2, bits);
    maxLimit -= 1; // 111...111

    range = maxLimit - minLimit + 1;

    mpz_class val(randGMP.get_z_range(range));
    val = val + minLimit - 1; // +1 car mpz_nextprime(val, val) ne teste pas si val est premier
    mpz_nextprime(val.get_mpz_t(), val.get_mpz_t());

    while(val > maxLimit)
    {
        val = randGMP.get_z_range(range);
        val = val + minLimit - 1;
        mpz_nextprime(val.get_mpz_t(), val.get_mpz_t());
    }
    return val;
} // randomPrime_n_bits
