#ifndef ELLIPTICCURVEPOINT_H
#define ELLIPTICCURVEPOINT_H

#include "gmpxx.h"

namespace nsEllipticCurve
{
    class EllipticCurvePoint
    {
        mpz_class m_X;
        mpz_class m_Y;
        mpz_class m_Z;
        mpz_class m_T;

    public:
        EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z, const mpz_class& T);
        void setX(const mpz_class& X);
        void setY(const mpz_class& Y);
        void setZ(const mpz_class& Z);
        void setT(const mpz_class& T);

        mpz_class getRealX(const mpz_class& p) const;
        mpz_class getRealY(const mpz_class& p) const;

        const mpz_class& getX() const;
        const mpz_class& getY() const;
        const mpz_class& getZ() const;
        const mpz_class& getT() const;

        void add(const EllipticCurvePoint& other, const mpz_class& d, const mpz_class& p);
        void pointMul(mpz_class mul, const mpz_class& d, const EllipticCurvePoint& neutralElement, const mpz_class& p);

        bool equals(const EllipticCurvePoint& other, const mpz_class& p) const;
    };
}

#endif // ELLIPTICCURVEPOINT_H
