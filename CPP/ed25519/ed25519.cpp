#include "ed25519.h"

// static members

const mpz_class nsSignature::Ed25519::_p("57896044618658097711785492504343953926634992332820282019728792003956564819949");
//const mpz_class nsSignature::Ed25519::_a("486662");
const mpz_class nsSignature::Ed25519::_d("37095705934669439343138083508754565189542113879843219016388785533085940283555");
const nsEllipticCurve::EllipticCurvePoint nsSignature::Ed25519::_B(mpz_class("15112221349535400772501151409588531511454012693041857206046113283949847762202"),
                                                                            mpz_class("46316835694926478169428394003475163141307993866256225615783033603165251855960"),
                                                                            1,
                                                                            mpz_class("46827403850823179245072216630277197565144205554125654976674165829533817101731"));
const nsEllipticCurve::EllipticCurvePoint nsSignature::Ed25519::_NEUTRAL_ELEMENT(0, 1, 1, 0);
const mpz_class nsSignature::Ed25519::_L("7237005577332262213973186563042994240857116359379907606001950938285454250989");
const size_t nsSignature::Ed25519::SIGNATURE_SIZE = 64;


/*********************************************************************************/

nsSignature::Ed25519::Ed25519()
{}

std::vector<uint8_t> nsSignature::Ed25519::encode(const mpz_class &value)
{
    if(value <= 0 /*|| value >= m_l - 1*/)
        throw std::invalid_argument("0 < value < L - 1");

    return numericUtils::bitsetToBytesVector(std::bitset<Ed25519PrivateKey::KEY_SIZE>(value.get_str(2)));
}

std::vector<uint8_t> nsSignature::Ed25519::encode(const nsEllipticCurve::EllipticCurvePoint &point)
{
    std::vector<uint8_t> tempVector(encode(point.getRealY(_p)));
    if((point.getRealX(_p)) % 2 == 1)
        tempVector.back() |= 128;
    return tempVector;
}

nsEllipticCurve::EllipticCurvePoint nsSignature::Ed25519::decode(const std::vector<uint8_t> &vec)
{
    if(vec.size() != 32)
        throw std::invalid_argument("invalid size");

    // First, interpret the string as an integer in little-endian
    mpz_class y(0);
    for(size_t i(vec.size()); i-- > 0; )
        y = y * 256 + vec[i];

    // Bit 255 of this number is the least significant bit of the x-coordinate
    mpz_class x_0(y >> 255);

    // The y-coordinate is recovered simply by clearing this bit
    mpz_class mask(1);
    mask <<= 255;
    mask -= 1;
    y &= mask;

    if(y >= _p)
        throw std::invalid_argument("cannot be decoded");

    mpz_class u(y * y - 1);
    mpz_class v(_d * y * y + 1);
    mpz_class x2(u);
    mpz_class temp;
    mpz_invert(temp.get_mpz_t(), v.get_mpz_t(), _p.get_mpz_t());
    x2 *= temp;

    temp = (_p + 3) / 8;

    mpz_class x;
    mpz_powm(x.get_mpz_t(), x2.get_mpz_t(), temp.get_mpz_t(), _p.get_mpz_t());

    if(((v * x * x) + u) % _p == 0)
        x = (x * mpz_class("19681161376707505956807079304988542015446066515923890162744021073123829784752")) % _p;
    else if(((v * x * x) - u) % _p != 0)
        throw std::invalid_argument("cannot be decoded");


    if(x == 0 && x_0 == 1)
        throw std::invalid_argument("cannot be decoded");

    if(x_0 != x % 2)
        x = _p - x;


    return nsEllipticCurve::EllipticCurvePoint(x, y, 1, (x*y) % _p);
}

std::vector<uint8_t> nsSignature::Ed25519::prepareMessage(const std::string &m) const
{
    if(m.size() % 2 != 0)
        throw std::invalid_argument("message size must be even");

    for(const char& c : m)
        if(!std::isxdigit(c))
            throw std::invalid_argument("non hexadecimal message");

    std::vector<uint8_t> result(m.size() / 2);

    for(size_t i(0); i < result.size(); ++i)
        result[i] = std::stoul(m.substr(2 * i, 2), nullptr, 16);

    return result;
}

void nsSignature::Ed25519::setPrivateKey(const std::shared_ptr<Ed25519PrivateKey> &privateKey)
{
    m_privateKey = privateKey;
}

void nsSignature::Ed25519::setPublicKey(const std::shared_ptr<nsSignature::Ed25519PublicKey> &publicKey)
{
    m_publicKey = publicKey;
}

std::vector<uint8_t> nsSignature::Ed25519::sign(const std::string& m) const
{
   /*
   The PureEdDSA signature of a message M under a private key k is the
   2*b-bit string ENC(R) || ENC(S).  R and S are derived as follows.
   First define r = H(h_b || ... || h_(2b-1) || M) interpreting 2*b-bit
   strings in little-endian form as integers in {0, 1, ..., 2^(2*b) -
   1}.  Let R = [r]B and S = (r + H(ENC(R) || ENC(A) || PH(M)) * s) mod
   L.  The s used here is from the previous section.
   */

    if(nullptr == m_privateKey)
        throw std::runtime_error("private key must be set");

    std::vector<uint8_t> mHex(prepareMessage(m));

    CryptoPP::SHA512 hash;
    ::byte digest[CryptoPP::SHA512::DIGESTSIZE];
    std::vector<uint8_t> message = m_privateKey->getKeyAsVector();

    hash.CalculateDigest(digest, (::byte*) message.data(), message.size());
    hash.Restart();

    ::byte concatPrefixAndM[CryptoPP::SHA512::DIGESTSIZE - 32 + mHex.size()];
    std::copy(digest + 32, digest + CryptoPP::SHA512::DIGESTSIZE, concatPrefixAndM);
    std::copy(mHex.begin(), mHex.end(), concatPrefixAndM + CryptoPP::SHA512::DIGESTSIZE - 32);

    hash.CalculateDigest(digest, (::byte*) concatPrefixAndM, sizeof(concatPrefixAndM));
    hash.Restart();

    mpz_class r(0);
    for(size_t i(sizeof(digest)); i-- > 0; )
        r = r * 256 + digest[i];
    r = r % _L;

    nsEllipticCurve::EllipticCurvePoint B2(_B);
    B2.pointMul(r, _d, _NEUTRAL_ELEMENT, _p);

    std::vector<uint8_t> encodedB2(encode(B2));
    message = encodedB2;

    std::pair<mpz_class, Ed25519PublicKey> pk(m_privateKey->getAssociatedPublicKey());
    std::vector<uint8_t> encoded2(pk.second.getKey());

    message.insert(message.end(), encoded2.begin(), encoded2.end());
    message.insert(message.end(), mHex.begin(), mHex.end());

    hash.CalculateDigest(digest, (::byte*) message.data(), message.size());
    hash.Restart();



    mpz_class numericDigest(0);
    for(size_t i(sizeof(digest)); i-- > 0; )
        numericDigest = 256 * numericDigest + digest[i];
    numericDigest %= _L;
    mpz_class S(r + numericDigest * pk.first);
    S = S % _L;


    std::vector<uint8_t> result2(encode(S));
    encodedB2.insert(encodedB2.end(), result2.begin(), result2.end());
    return encodedB2;
}

bool nsSignature::Ed25519::verify(const std::string &m, const std::vector<uint8_t> &signature) const
{
    if(nullptr == m_publicKey)
        throw std::runtime_error("private key must be set");

    if(signature.size() != SIGNATURE_SIZE)
        throw std::invalid_argument("invalid signature size");

    std::vector<uint8_t> mHex(prepareMessage(m));

    std::vector<uint8_t> firstHalf(SIGNATURE_SIZE / 2), secondHalf(SIGNATURE_SIZE / 2);
    std::copy(signature.begin(), signature.begin() + (SIGNATURE_SIZE / 2), firstHalf.begin());
    std::copy(signature.begin() + (SIGNATURE_SIZE / 2), signature.end(), secondHalf.begin());

    try
    {
        nsEllipticCurve::EllipticCurvePoint R(decode(firstHalf));
        nsEllipticCurve::EllipticCurvePoint A(decode(m_publicKey->getKey()));

        mpz_class S(0);
        for(size_t i(secondHalf.size()); i-- > 0; )
            S = 256 * S + secondHalf[i];

        std::vector<uint8_t> key(m_publicKey->getKey());
        std::vector<uint8_t> concat(firstHalf.size() + key.size() + mHex.size());
        std::copy(firstHalf.begin(), firstHalf.end(), concat.begin());
        std::copy(key.begin(), key.end(), concat.begin() + firstHalf.size());
        std::copy(mHex.begin(), mHex.end(), concat.begin() + firstHalf.size() + key.size());

        CryptoPP::SHA512 hash;
        ::byte digest[CryptoPP::SHA512::DIGESTSIZE];

        hash.CalculateDigest(digest, (::byte*) concat.data(), concat.size());

        mpz_class k(0);
        for(size_t i(sizeof(digest)); i-- > 0; )
            k = 256 * k + digest[i];
        k = k % _L;


        //check [S]B = R + [k]A
        nsEllipticCurve::EllipticCurvePoint sB(_B);
        sB.pointMul(S, _d, _NEUTRAL_ELEMENT, _p);
        A.pointMul(k, _d, _NEUTRAL_ELEMENT, _p);
        nsEllipticCurve::EllipticCurvePoint sumRkA(R);
        sumRkA.add(A, _d, _p);

        return sumRkA.equals(sB, _p);
    }
    catch(const std::invalid_argument& e)
    {
        (void)e;
        return false;
    }
}
