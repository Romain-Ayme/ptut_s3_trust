#ifndef ED25519_H
#define ED25519_H

#include <memory>
#include <exception>
#include <vector>

#include "gmpxx.h"
#include "cryptopp/sha.h"
#include "cryptopp/hex.h"

#include "ellipticcurvepoint.h"
#include "ed25519privatekey.h"
#include "ed25519publickey.h"

#include "numericutils.hpp"

namespace nsSignature
{
    class Ed25519PrivateKey;
    class Ed25519
    {
        std::shared_ptr<Ed25519PrivateKey> m_privateKey;
        std::shared_ptr<Ed25519PublicKey> m_publicKey;

        mpz_class m_b; // 2^(b - 1) > p and the hash function must produce 2*b output bits

    public:

        static const nsEllipticCurve::EllipticCurvePoint _B; // generator
        static const mpz_class _d;
        static const mpz_class _p; // odd prime power
        static const mpz_class _a;
        static const mpz_class _L;
        static const nsEllipticCurve::EllipticCurvePoint _NEUTRAL_ELEMENT;
        static const size_t SIGNATURE_SIZE;

        Ed25519();

        void setPrivateKey(const std::shared_ptr<Ed25519PrivateKey>& privateKey);
        void setPublicKey(const std::shared_ptr<Ed25519PublicKey>& publicKey);
        std::vector<uint8_t> sign(const std::string& m) const;
        bool verify(const std::string& m, const std::vector<uint8_t>& signature) const;

        static std::vector<uint8_t> encode(const mpz_class& value);
        static std::vector<uint8_t> encode(const nsEllipticCurve::EllipticCurvePoint &point);

        static nsEllipticCurve::EllipticCurvePoint decode(const std::vector<uint8_t> &vec);

    private:

        std::vector<uint8_t> prepareMessage(const std::string& m) const;
    }; // ed25519
} // nsSignature

#endif // ED25519_H
