#ifndef ELLIPTICCURVE_H
#define ELLIPTICCURVE_H

#include "ellipticcurvepoint.h"

namespace nsEllipticCurve
{
    class EllipticCurve
    {

    public:
        static const EllipticCurvePoint neutralElement;

    public:
        EllipticCurve();
    };
}

#endif // ELLIPTICCURVE_H
