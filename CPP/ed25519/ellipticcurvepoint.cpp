#include <exception>

#include "ellipticcurvepoint.h"

using namespace std;


nsEllipticCurve::EllipticCurvePoint::EllipticCurvePoint(const mpz_class& X, const mpz_class& Y, const mpz_class& Z, const mpz_class& T)
    : m_X(X), m_Y(Y), m_Z(Z), m_T(T)
{}

void nsEllipticCurve::EllipticCurvePoint::setX(const mpz_class &X)
{
    m_X = X;
}

void nsEllipticCurve::EllipticCurvePoint::setY(const mpz_class &Y)
{
    m_Y = Y;
}

void nsEllipticCurve::EllipticCurvePoint::setZ(const mpz_class &Z)
{
    m_Z = Z;
}

void nsEllipticCurve::EllipticCurvePoint::setT(const mpz_class &T)
{
    m_T = T;
}

const mpz_class& nsEllipticCurve::EllipticCurvePoint::getX() const
{
    return m_X;
}

const mpz_class& nsEllipticCurve::EllipticCurvePoint::getY() const
{
    return m_Y;
}

const mpz_class& nsEllipticCurve::EllipticCurvePoint::getZ() const
{
    return m_Z;
}

const mpz_class& nsEllipticCurve::EllipticCurvePoint::getT() const
{
    return m_T;
}

mpz_class nsEllipticCurve::EllipticCurvePoint::getRealX(const mpz_class& p) const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), p.get_mpz_t());
    mpz_class x(m_X * invZ % p);
    if(x < 0)
        x += p;
    return x;
}

mpz_class nsEllipticCurve::EllipticCurvePoint::getRealY(const mpz_class& p) const
{
    mpz_class invZ;
    mpz_invert(invZ.get_mpz_t(), m_Z.get_mpz_t(), p.get_mpz_t());
    mpz_class y(m_Y * invZ % p);
    if(y < 0)
        y += p;
    return y;
}

void nsEllipticCurve::EllipticCurvePoint::add(const nsEllipticCurve::EllipticCurvePoint &other, const mpz_class& d, const mpz_class& p)
{
    mpz_class A(((m_Y - m_X) * (other.m_Y - other.m_X)) % p);
    mpz_class B(((m_Y + m_X) * (other.m_Y + other.m_X)) % p);
    mpz_class C((m_T * 2 * d * other.m_T) % p);
    mpz_class D((m_Z * 2 * other.m_Z) % p);

    if(A < 0) A += p;
    if(B < 0) B += p;
    if(C < 0) C += p;
    if(D < 0) D += p;

    mpz_class E(B - A);
    mpz_class F(D - C);
    mpz_class G(D + C);
    mpz_class H(B + A);

    m_X = E * F;
    m_Y = G * H;
    m_T = E * H;
    m_Z = F * G;
}

void nsEllipticCurve::EllipticCurvePoint::pointMul(mpz_class mul, const mpz_class& d, const EllipticCurvePoint& neutralElement, const mpz_class& p)
{
    nsEllipticCurve::EllipticCurvePoint P = *this;
    *this = neutralElement;

    while(mul > 0)
    {
        if(mul % 2 == 1)
            this->add(P, d, p);
        P.add(P, d, p);
        mul >>= 1 ;
    }
}

bool nsEllipticCurve::EllipticCurvePoint::equals(const nsEllipticCurve::EllipticCurvePoint &other, const mpz_class &p) const
{
    // x = X/Z et y = Y/Z
    // donc (x1 == x2) ^ (y1 == y2)   <=>   (X1/Z1 == X2/Z2) ^ (Y1/Z1 == Y2/Z2)
    // <=>  (X1*Z2 == X2*Z1) ^ (Y1*Z2 == Y2*Z1)

    if(((m_X * other.m_Z) - (other.m_X * m_Z)) % p != 0)
        return false;

    if(((m_Y * other.m_Z) - (other.m_Y * m_Z)) % p != 0)
        return false;

    return true;
}
