#ifndef ED25519PUBLICKEY_H
#define ED25519PUBLICKEY_H

#include <vector>

#include "gmp.h"
#include "gmpxx.h"

#include "ellipticcurvepoint.h"
#include "ed25519key.h"

namespace nsSignature
{
    class Ed25519PublicKey : public Ed25519Key
    {
        nsEllipticCurve::EllipticCurvePoint m_key;
    public:
        Ed25519PublicKey(const nsEllipticCurve::EllipticCurvePoint& key);
        const nsEllipticCurve::EllipticCurvePoint& getPoint() const;
        std::vector<uint8_t> getKey() const;
        // get point
        // get key (encoded point)
    };
}

#endif // ED25519PUBLICKEY_H
