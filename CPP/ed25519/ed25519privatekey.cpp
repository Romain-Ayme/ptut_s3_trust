#include <algorithm>
#include "ed25519privatekey.h"

/*
    paramètre en hexadecimal
*/
nsSignature::Ed25519PrivateKey::Ed25519PrivateKey(const std::string& keyString)
{
    if(KEY_SIZE != keyString.size()*4)
        throw std::invalid_argument("invalid key size");

    for(size_t i(0); i < keyString.size(); ++i)
        if(std::isxdigit(keyString[i]) == 0)
            throw std::invalid_argument("non hexadecimal key");

    mpz_class temp(keyString, 16);
    std::string binStr(temp.get_str(2));
    if(binStr.size() != KEY_SIZE)
        binStr = std::string(KEY_SIZE - binStr.size(), '0') + binStr;
    for(size_t i(0); i < binStr.size(); ++i)
        m_key[m_key.size() - 1 - i] = (binStr[i] == '1' ? true : false);
}

std::pair<mpz_class, nsSignature::Ed25519PublicKey> nsSignature::Ed25519PrivateKey::getAssociatedPublicKey() const
{
    std::vector<uint8_t> buffer(getKeyAsVector());

    CryptoPP::SHA512 hash;
    ::byte digest[CryptoPP::SHA512::DIGESTSIZE];

    hash.CalculateDigest(digest, (::byte*) buffer.data(), buffer.size());
    hash.Restart();

    std::vector<uint8_t> buffer2(32);
    std::copy(digest, digest + 32, buffer2.begin());

    mpz_class s(0);
    mpz_class power(1);
    for(size_t i(0); i < buffer2.size(); ++i)
    {
        s += power * buffer2[i];
        power *= 256;
    }

    mpz_class temp(1);
    temp <<= 254;
    s &= (temp - 8);
    s |= temp;

    nsEllipticCurve::EllipticCurvePoint B = nsSignature::Ed25519::_B;
    B.pointMul(s,
               Ed25519::_d,
               Ed25519::_NEUTRAL_ELEMENT,
               Ed25519::_p);

    return {s, Ed25519PublicKey(B)};
}

std::string nsSignature::Ed25519PrivateKey::getAsString() const
{
    return m_key.to_string();
}

std::vector<uint8_t> nsSignature::Ed25519PrivateKey::getKeyAsVector() const
{
    return numericUtils::bitsetToBytesVectorInvert(m_key);
}
