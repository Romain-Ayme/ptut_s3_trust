#include "ed25519publickey.h"
#include "ed25519.h"

nsSignature::Ed25519PublicKey::Ed25519PublicKey(const nsEllipticCurve::EllipticCurvePoint &key)
    : m_key(key)
{}

const nsEllipticCurve::EllipticCurvePoint& nsSignature::Ed25519PublicKey::getPoint() const
{
    return m_key;
}

std::vector<uint8_t> nsSignature::Ed25519PublicKey::getKey() const
{
    return nsSignature::Ed25519::encode(m_key);
}
