TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../include
INCLUDEPATH += ../../include/ed25519

LIBS += -lgmpxx -lgmp
LIBS += -lcryptopp

SOURCES += \
        ed25519.cpp \
        ed25519key.cpp \
        ed25519privatekey.cpp \
        ed25519publickey.cpp \
        ellipticcurve.cpp \
        ellipticcurvepoint.cpp \
        main.cpp

HEADERS += \
    ed25519.h \
    ed25519.h \
    ed25519key.h \
    ed25519privatekey.h \
    ed25519publickey.h \
    ellipticcurve.h \
    ellipticcurvepoint.h \
    numericutils.hpp
