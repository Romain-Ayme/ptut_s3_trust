#ifndef NUMERICUTILS_HPP
#define NUMERICUTILS_HPP

#include <vector>
#include <bitset>
#include <cstdint>

namespace numericUtils
{
    template<size_t size>
    std::vector<uint8_t> bitsetToBytesVector(const std::bitset<size>& bitset);

    template<size_t size>
    std::vector<uint8_t> bitsetToBytesVectorInvert(const std::bitset<size>& bitset);
}

namespace
{

}


template<size_t size>
std::vector<uint8_t> numericUtils::bitsetToBytesVector(const std::bitset<size>& bitset)
{
    uint8_t pow(1);
    std::vector<uint8_t> result((bitset.size() / 8) + (bitset.size() % 8 != 0 ? 1 : 0), 0);
    for(size_t i(0); i < bitset.size(); ++i)
    {
        if(i % 8 == 0 && i != 0)
            pow = 1;

        if(true == bitset[i])
            result[i / 8] = result[i / 8] | pow;

        pow = pow << 1;
    }
    return result;
}

template<size_t size>
std::vector<uint8_t> numericUtils::bitsetToBytesVectorInvert(const std::bitset<size>& bitset)
{
    uint8_t pow(1);
    std::vector<uint8_t> result((bitset.size() / 8) + (bitset.size() % 8 != 0 ? 1 : 0), 0);
    for(size_t i(0); i < bitset.size(); ++i)
    {
        if(i % 8 == 0 && i != 0)
            pow = 1;

        if(true == bitset[i])
            result[result.size() - 1 - (i / 8)] = result[result.size() - 1 - (i / 8)] | pow;

        pow = pow << 1;
    }
    return result;
}

#endif // NUMERICUTILS_H
