#ifndef ED25519PRIVATEKEY_H
#define ED25519PRIVATEKEY_H

#include <bitset>
#include <random>

#include "gmp.h"
#include "gmpxx.h"

#include "ed25519.h"
#include "ed25519key.h"
#include "ed25519publickey.h"
#include "ellipticcurvepoint.h"

#include "numericutils.hpp"

namespace nsSignature
{
    class Ed25519PrivateKey : public Ed25519Key
    {
    public:
        static constexpr size_t KEY_SIZE = 256;

    private:
        std::bitset<KEY_SIZE> m_key;

    public:
        Ed25519PrivateKey(const std::string& keyString);
        //Ed25519PublicKey getAssociatedPublicKey() const;
        std::string getAsString() const;
        std::vector<uint8_t> getKeyAsVector() const;
        std::pair<mpz_class, Ed25519PublicKey> getAssociatedPublicKey() const;
    };
}

#endif // ED25519PRIVATEKEY_H
