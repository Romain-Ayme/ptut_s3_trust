chaque répertoire dans myLibraries/src est un projet Qt et contient un fichier main.cpp de présentation très rapide des fonctionnalités implémentées.

génération des bibliothèques :

make (ou make all)


**********
pour effacer les fichiers intermédiaires générés (*.o) :
	make clean

pour effacer les bibliothèques générées :
	make mrproper
**********
