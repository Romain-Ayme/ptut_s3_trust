\documentclass[class=article, crop=false]{standalone}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{url}

\begin{document}

\section{ECDSA}
\subsection{Introduction}

ECDSA est un algorithme de signature utilisé dans le cadre du protocole DNSSEC, reposant sur les sous-groupes cycliques des courbes elliptiques, et sa sécurité dépend de la difficulté du problème du logarithme discret sur les courbes elliptiques. Il assure la génération des clés privées et des clés publiques (cryptographie asymétrique) nécessaire aux signatures numériques. ECDSA permet en outre d'utiliser des clés plus courtes que celles employées par RSA, ce qui permet d'accélérer le processus de signature. Par exemple, la sécurité assurée par une clé de 3072 bits par RSA est similaire  à celle permise par une clé de 256 bits avec ECDSA (nous discuterons cette différence dans la partie consacrée à la règle verte).

https://cryptobook.nakov.com/digital-signatures/ecdsa-sign-verify-messages

\subsection{Prérequis}

\subsubsection{Groupe}

Un groupe en mathématiques se compose de deux choses: un Ensemble \(G\) ainsi qu'une opération pour ce groupe noté généralement \(\bullet\). \\Cette opération doit respecter quatre propriétés :   

\begin{itemize}
    \item Clôture \\
    \(\forall x, y \in G, x \bullet y \in G\)
    
    \item Associativité \\
    \(\forall x, y, z \in G, (x \bullet y) \bullet z = x \bullet (y \bullet z)\)
    
    \item Élément neutre \\
    \(\exists e \in G, \forall x \in G, x \bullet e = e \bullet x = x\)
    
    \item Inverse \\
    \(\forall x \in G, \exists y \in G, x \bullet y = y \bullet x = e\)
    
    \item Si \(\forall x, y \in G, x \bullet y = y \bullet x\) alors il s'agit d'un groupe abélien (communtatif). Si cette propriété n'est pas respectée, il s'agit tout de même d'un groupe (les 4 premières propriétés sont essentielles).
\end{itemize}

On qualifie de sous-groupe, un groupe inclus dans un autre, puisque le sous-groupe vérifie les propriétés énoncées ci-dessus, il contient nécessairement l'élément neutre du groupe dans lequel il est inclus.

Voici quelques exemples de groupes : \((\mathbb{Z}, +)\), \((\mathbb{R}, +)\), \((\mathbb{Z}, +)\), \(((\frac{\mathbb{Z}}{p\mathbb{Z}})^{\times}, \times)\) avec \(p\) un nombre premier.

\paragraph{Groupes cycliques}

Soit un groupe \((G, \bullet)\), ce groupe est monogène si il existe un élément \(g \in G\) tel que pour tout \(x \in G\), il existe \(k \in \mathbb{Z}\) tel que \(x = g^k\). Dans ce cas, \(g\) est un générateur du groupe. \\

Tout groupe monogène est abélien. \\
Prenons \((G, \bullet)\) un groupe monogène, \(g\) un générateur, et \(x, y \in G\). Il existe \(k, t \in \mathbb{Z}\) tel que \(x = g^k\) et \(y = g^t\).
Donc \(x \bullet y = g^k \bullet g^t = g^{k + t}\) Mais l'addition est commutative dans \(\mathbb{Z}\) donc \(g^{k + t} = g^{t + k} = g^t \bullet g^k = y \bullet x\). \\

De plus, on dit qu'un groupe est cyclique s'il est monogène et fini.

\subsubsection{Introduction sur les courbes elliptiques}

ECDSA utilise la cryptographie basée sur les courbes elliptiques. Dans cet algorithme nous travaillons dans un sous-groupe cyclique d'une courbe elliptique. Les courbes recommandées sont listées dans le document à cette adresse : \\
\url{https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.186-4.pdf} \\

Une courbe elliptique est une courbe dont l'équation est de la forme : \[y^2 = x^3 + ax + b\] (a et b sont des nombres réels) et se représente graphiquement comme ceci:

\begin{center}
\includegraphics[width=7cm, height=4cm]{../ressources/Courbe_Elliptique.png}
\end{center}

Le discriminant de la courbe s'écrit de la forme \(\Delta = -16(4a^3 +27b^2)\). \\
Ce discriminant doit impérativement être non nul. \\

Il permet de définir la forme de la courbe : 
\begin{itemize}
  \item si \(\Delta > 0\), la forme de la courbe sera celle de la figure 1
  \item si \(\Delta < 0\), la forme de la courbe sera celle de la figure 2 
\end{itemize}

Sur de telles courbes, on définit l'addition de deux points ainsi : \\
Prenons la courbe ci-dessous. Sur cette courbe C, on choisit deux points P et Q puis on trace la droite D qui passe par ces deux points. Un troisième point R se crée à l’intersection de la droite D et la courbe C. R’ est le symétrique de R par rapport à l’axe des abscisses. On dit alors que P + Q = R’.

\begin{center}
\includegraphics[width=6cm, height=5cm]{../ressources/Courbe_Elliptique_Symetrie.png}
\end{center}



\textbf{Exceptions} : droite tangente (2), droite verticale (3 et 4)

\begin{center}
\includegraphics[width=10cm, height=4cm]{../ressources/Courbe_Elliptique_Exception.png}
\end{center}


Pour pouvoir utiliser ces courbes dans la cryptographie, il faut avant tout ne garder que les points dont l’abscisse et l’ordonnée sont des entiers et aussi, utiliser l'arithmétique modulaire afin de limiter l’abscisse et l’ordonnée maximale de chaque point, ainsi chaque point sera exprimé modulo \(p\) (un nombre premier supérieur à 3). Nous nous intéressons donc aux points \(P = (x, y)\) avec \(x, y \in \frac{\mathbb{Z}}{p\mathbb{Z}}\) et \(p\) premier supérieur à 3. De ce fait, nous travaillons dans le groupe commutatif \[(E(\frac{\mathbb{Z}}{p\mathbb{Z}}), +)\]
avec
\[E(\frac{\mathbb{Z}}{p\mathbb{Z}}) = \{(x, y) \in (\frac{\mathbb{Z}}{p\mathbb{Z}})^2 : y^2 = x^3 + ax + b\} \cup \{O\}\]

\(O\) est le point à l'infini, qui sert d'élément neutre pour l'addition des points. Il peut s'imaginer intuitivement comme le point d'intersection de toutes les droites verticales. \\

\begin{center}
\includegraphics[width=6cm, height=8cm]{../ressources/Point_infini.png}
\end{center}

Voici à quoi peut ressembler une représentation graphique si l'on ne considère que les points dont les coordonnées \(x, y\) sont dans \(\frac{\mathbb{Z}}{p\mathbb{Z}}\) (ici on prend \(p = 23\)).

\begin{center}
\includegraphics[scale=0.4]{../ressources/Zsur23Z_exemple_representation_graphique.png}
\end{center}

Puisque nous avons un groupe, alors les propriétés suivantes sont vérifiées : \\
\begin{itemize}
    \item Il existe un élément neutre, qui est \(O\)
    
    \item pour tout \(P, Q, R \in E(\frac{\mathbb{Z}}{p\mathbb{Z}}), P + (Q + R) = (P + Q) + R\).
    
    \item Pour tout point \(P \in E(\frac{\mathbb{Z}}{p\mathbb{Z}})\), il existe \(Q \in E(\frac{\mathbb{Z}}{p\mathbb{Z}})\) tel que \(P + Q = Q + P = O\). \(Q\) est l'inverse de \(P\) et on le note \(-P\). Graphiquement, ce point se trouve sur la même droite verticale que le point \(P\).
    
    \item Pour tout points \(P, Q \in E(\frac{\mathbb{Z}}{p\mathbb{Z}}), P + Q \in E(\frac{\mathbb{Z}}{p\mathbb{Z}})\)
\end{itemize}

\subsubsection{Algorithme d’addition de points}

Soient \(P_1, P_2 \in E(\frac{\mathbb{Z}}{p\mathbb{Z}})\) deux points avec \(P_1 = (x_1, y_1)\) et \(P_2 = (x_2, y_2)\). Cherchons \(P_1 + P_2\).

Dans un premier temps, si \(x_1 = x_2\) et \(y_1 = -y_2\), alors, \(P_1 + Q_2 = O\). \\

\textit{En prenant \(P\) et \(Q\) dans \(E(\mathbb{R})\), ce cas particulier est plus simple à comprendre. En effet, cela signifie que les points sont situés sur la même droite verticale, le troisième point est donc le point à l'infini, qui est son propre symétrique, cela signifie bien que \(P + Q = O\).} \\

Si \(x_1 = x_2\) et \(y_1 = y_2 \neq 0\) alors \[m = \frac{3x{_1}^2 + a}{2y_1}\]
qui est le coefficient directeur de la tangente.

Sinon on a
\[m = \frac{y_1 - y_2}{x_1 - x_2}\]
qui est la pente de la droite qui passe par les deux points. \\

Soient \(X, Y \in \frac{\mathbb{Z}}{p\mathbb{Z}}\). \\

tels que :
\[X = m^2 - x_1 - x_2\]
\[Y = y_1 + m(X - x_1)\]


\(P_1 + P_2 = (X, -Y)\)

\paragraph{Exemple}

Prenons \(p = 5\), nous travaillons donc dans \(\frac{\mathbb{Z}}{5\mathbb{Z}}\). L'équation de la courbe est \(y^2 = x^3 + x + 1\).
Soient \(P = (3, 1)\), \(Q = (3, 4)\) et \(R = (4, 2)\). \\

Commençons par vérifier que ces points sont bien sur la courbe. \\
\[y{_p}^2 = 1 \equiv 1 \mod 5\]
\[x{_p}^3 + x_p + 1 = 27 + 3 + 1 = 31 \equiv 1 \mod 5\]
\\

\[y{_q}^2 = 16 \equiv 1 \mod 5\]
\[x{_q}^3 + x_q + 1 = 27 + 3 + 1 = 31 \equiv 1 \mod 5\]
\\

\[y{_r}^2 = 4 \equiv 4 \mod 5\]
\[x{_r}^3 + x_r + 1 = 64 + 4 + 1 = 69 \equiv 4 \mod 5\]

Maintenant, calculons \(P + Q\) et \(P + R\). \\

\(x_p = x_q = 3\) et \(y_p = 1 = -4 = -y_q\) donc \(P + Q = O\) et \(P = -Q\). \\

\(x_p \neq x_r\) donc \(m = \frac{1 - 2}{3 - 4} = \frac{-1}{-1} = \frac{1}{1}\), ce qui l'inverse multiplicatif de \([1]\) dans \(\frac{\mathbb{Z}}{5\mathbb{Z}}\) qui vaut \([1]\).
\[X = 1^2 - 3 - 4 = -6 \equiv 4 \]
\[Y = 1 + 1(4 - 3) \equiv 2\]
\(P + R = (X, -Y) = (4, -2) = (4, 3)\)


\paragraph{Multiplication d'un point par un scalaire}

Soient \(P \in E(\frac{\mathbb{Z}}{p\mathbb{Z}})\) et \(n \in \mathbb{N^*}\). Calculer \(nP\) revient à calculer la somme \(P + P + ... + P + P\) (n fois). Cependant, cette méthode est très lente est il est tout à fait possible de faire mieux sans pour autant compliquer significativement le processus, Voici l'une des possibilités. \\

On utilise l’algorithme “double-et-ajouter” , le principe est le suivant :

Pour calculer \(nP\), on cherche la représentation binaire de \(n\). Il est possible d'opérer par divisions successives mais dans une implémentation il serait probablement plus efficace de travailler directement avec des masques et des opérations logiques. Le simple fait de stocker la valeur \(1\) dans la variable \(i\) et de calculer le résultat du ET logique avec la valeur de \(n\) donnera son bit de poids faible. Il suffit par la suite d'effectuer des décalages de bits pour récupérer les autres bits de \(n\) ainsi les valeurs de \(i\) seront successivement 1, 2, 4, 8... (puissances de 2). \\

Prenons par exemple \(n = 19\) et cherchons sa représentation binaire. 
\[19 = 10011_2 = 16 + 2 + 1 = 2^4 + 2^1 + 2^0\]
Ce qui nous donne
\[19P = 2^4P + 2^1P + 2^0P = 2(2(2(2P))) + 2P + P\]
On a ainsi diminué le nombre d'opérations, en effet, il fallait dans un premier temps calculer \(18\) additions, nous avons réduit ce nombre à \(7\) (2 additions et 5 opérations de doublement de point qui sont très similaires aux additions). \\

\textit{Il existe d'autres façons d'effectuer ce type d'opération, une autre méthode est disponible à l'annexe D.3.2 de ce document : } \\
\url{https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.202.2977&rep=rep1&type=pdf}

\subsection{Fonctionnement}
\subsubsection{Génération des clés}

Nous avons dans un premier temps introduit les éléments de base nécessaires à la compréhension du fonctionnement de l'algorithme. Passons maintenant à la description de la phase de génération de clés. \\

Soient p, A, B, G et n tels que :

\begin{itemize}
    \item \(p\) est un nombre premier supérieur à 3.
    \item \(A\) et \(B\) dans \(\frac{\mathbb{Z}}{p\mathbb{Z}}\)
    \item On prend une courbe elliptique C dont l'équation est : \(y^2 = x^3 + Ax + B\) (on travaille modulo \(p\))
    \item \(G \neq O\) un point de la courbe dont l'ordre \(n\) doit être premier.
    \item \(nG = O\) (\(n\) est l'ordre de \(G\), ce qui correspond au plus petit entier \(k\) tel que \(kG = O\) avec \(O\) l'élément neutre du groupe dans lequel on travaille, ici, c'est le point à l'infini)
\end{itemize}

\textit{Notons que \(n\) doit être strictement supérieur à \(2^{160}\) et à \(4\sqrt{p}\).}

Pour les signature avec ECDSA, la clé privée est un entier et la clé publique est un point sur la courbe \(C\) que nous avons défini ci-dessus. \\

La génération d'une clé privée est simple, il suffit de choisir "aléatoirement" un entier \(d\) tel que \(1 \leq d \leq n - 1\). \\

La clé publique, est le point \(Q = dG\) de la courbe. \\

On a donc la paire de clés \((Q, d)\) avec \(Q\) la clé publique et \(d\) la clé privée. \\


\textbf{Remarque} il est très facile de calculer \(Q\) en ayant connaissance de \(d\) et \(G\) mais il est en revanche très difficile de retrouver \(d\) à partir de \(G\) et \(Q\) en un temps raisonnable. Nous sommes ici confrontés au problème du logarithme discret sur les courbes elliptiques (nous évoquons le problème du logarithme discret dans la partie sur le protocole Diffie-Hellman et nous parlerons de la variante du problème sur les courbes elliptiques au S4).

\subsubsection{Calcul de la signature d’un message}

Dans cette partie nous conservons les notations introduites lors de la génération des clés. \\
Une fois la paire de clé générée, nous arrivons à l'étape de signature. Voici son fonctionnement :

\begin{itemize}
  \item Soit \(m\) les données à signées traitées par la fonction de hachage SHA-256.
  \item Soit \(k\) un entier entre \(1\) et \(n - 1\) généré aléatoirement. Cette valeur doit être régénérée à chaque nouvelle signature.
  \item Calculer le point \(kG = (x_1, y_1)\). Soit \(r = x_1 \mod n\), Si \(r = 0\) retourner à la deuxième étape.
  \item calculer \(k^{-1} \mod n\)
  \item Soit \(s = k^{-1} (m + dr) \mod n\). Si \(s = 0\) retourner à la deuxième étape.
\end{itemize}

La signature est donnée par le couple \((r, s)\).

\textit{Pour calculer \(k^{-1}\) il est possible d'utiliser l'algorithme d'Euclide étendu ou de calculer \(k^{n - 2}\) qui est l'inverse de \(k\) modulo \(n\) (théorème d'Euler car \(n\) est premier donc premier avec \(k < n\))}

\subsubsection{Vérification d’une signature}

Voici maintenant les étapes de la vérification de signature \((r, s)\) :

\begin{itemize}
  \item Vérifier que r et s sont dans [1, n - 1]
  \item Calculer \(w = s^{-1} \mod n\) ainsi que \(m\) en appliquant la fonction de hachage sur les données.
  \item \(u_1 = mw \mod n\) et \(u_2 = rw \mod n\)
  \item \(u_1G + u_2Q = (x_1, y_1)\), Si ce point est le point à l'infini, la signature est incorrecte
  \item \(v = x_1 \mod n\)
  \item Si \(v = r\) alors la signature est correcte, sinon le message peut être rejeté car invalide.
\end{itemize}

Lors de la vérifications, on cherche \(u_1G + u_2Q = (mw)G + (rw)Q\). \\
Or, \(Q\) est la clé publique et on a \(Q = dG\).
\[u_1G + u_2Q = u_1G + u_2dG\]
\[= (ms^{-1} \mod n)G + (rs^{-1} \mod n)dG\]
\[= ((m + rd)s^{-1}) \mod n)G\]

De plus, \(s^{-1} \mod n = (k^{-1}(m + dr))^{-1} \mod n\). \\
De ce fait, on a

\[= ((m + rd)(k^{-1}(m + dr))^{-1}) \mod n)G\]
\[= ((m + rd)k(m + dr)^{-1}) \mod n)G\]
\[= (k \mod n)G\]

Si la signature et les données sont correctes, on trouve bien la même chose que lors de l'étape de signature. \\

\paragraph{Références \\}
\url{https://www.techno-science.net/glossaire-definition/Courbe-elliptique.html} \\
\url{https://e-ducat.fr/links/ecdsa/} \\
\url{https://www.miximum.fr/blog/cryptographie-courbes-elliptiques-ecdsa/} \\
\url{https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.202.2977&rep=rep1&type=pdf} \\
\url{https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.186-4.pdf} \\

\end{document}
